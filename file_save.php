<?php
// Save data permanently.
// (c) 2015 Pedro A. Ortega <pedro.ortega@gmail.com>
// (c) 2015 David N. White  <davey@autistic.org>

// The $_POST[] array will contain the passed in filename and data.
// The directory "data" is writable by the server (chmod 777).
$filename = "data/".$_POST['filename'];
$data = $_POST['filedata'];

// Write the file to disk.
file_put_contents($filename, $data, FILE_APPEND);
?>

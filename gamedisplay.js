/******************************************************************************
 *
 * GAME DISPLAY
 *
 * This file should contain the graphical elements needed for the game.
 *
 * (c) 2015-2016 Pedro A. Ortega <pedro.ortega@gmail.com>
 * (c) 2015      David N. White  <davey@autistici.org>
 *****************************************************************************/

/******************************************************************************
 * DATA, CONSTANTS AND AUXILIARY FUNCTIONS
 *****************************************************************************/

/*
 * Create array of given length and fill it with a given value.
 */
function ArrayWithValue(length, value) {
  var arr = [], i =0;
  arr.length = length;
  while (i < length) { arr[i++] = value; }
  return arr;
}

/*
 * Round the value of a real number to 3 decimal places.
 */
function _P(value) {
  return Math.round(1000 * value) / 1000;
}

// Color palette.
lightColor = { color : '#000000', on : '#ffffff', off : '#000000', idle : '#ffffff'};

backColor     = "#999999";
plateBorder   = "#cccccc";
plateColor    = "#ffffff";
plateFlash    = "#ff0000";
switchHover   = "#ffffff";
controlsColor = "#cccccc";
statusAlert   = "#ff0000";
statusNormal  = "#000000";
timerColor    = "#eeeeee";
cellOn        = "#eeeeee";
cellOff       = "#000000";

/******************************************************************************
 * VIEW
 *****************************************************************************/

/*
 * Light bulb : displays a light bulb that
 * can be in an on/off state.
 */
function Light(paper, x, y, radius, light) {

  // Sets the light of the bulb.
  this.setLight = function(light) {
    this.light = light;
    if (light > 0) {
      color = lightColor.on;
      strokeColor = lightColor.color;
    } else if (light < 0) {
      color = lightColor.off;
      strokeColor = lightColor.color;
    } else {
      color = lightColor.idle;
      strokeColor = lightColor.idle;
    }
    this.bulb.attr({
      "fill":   color,
      "stroke": strokeColor,
      "stroke-width": _P(0.2*radius),
    });
  };

  // Hide/Show.
  this.hide = function() { this.bulb.hide(); }
  this.show = function() { this.bulb.show(); }

  // Initialize fields.
  this.paper  = paper;
  this.x      = x;
  this.y      = y;
  this.radius = radius;
  this.bulb = paper.circle(_P(x), _P(y), _P(radius));
  this.setLight(light);

  return this;
}

/*
 * Plate : displays a plate with light bulbs.
 */
function Plate(paper, x, y, radius, nStates, clause) {

  // Sets the clause of the plate.
  this.setPlate = function(clause) {

    // Erase all lights first.
    for (var n=0; n<this.lights.length; n++) {
      this.lights[n].setLight(0);
    }
    // Now set the lights that are active in the clause.
    for (var n=0; n<clause.length; n++) {
      lightId = Math.abs(clause[n]) - 1;
      this.lights[lightId].setLight(clause[n]);
    }
  }

  // Sets the flashing state of the plate.
  this.setFlashing = function(flash) {
    this.plate.attr({
      "fill": flash? plateFlash : plateColor,
    });
  }

  // Hide/Show.
  this.hide = function() {
    for (var n=0; n<this.lights.length; n++) {
      this.lights[n].hide();
    }
    this.plate.hide();
  };
  this.show = function() {
    for (var n=0; n<this.lights.length; n++) {
      this.lights[n].show();
    }
    this.plate.show();
  };

  this.paper  = paper;
  this.x      = x;
  this.y      = y;
  this.radius = radius;

  this.plate = paper.circle(_P(x), _P(y), _P(radius));
  this.plate.attr({
    "fill"  : plateColor,
    "stroke": plateBorder,
    "stroke-width": _P( 0.05*radius ),
  });

  this.lights = Array();
  var omega = 2*Math.PI / nStates;
  var sinO2 = Math.sin(omega/2);
  var rBulb = this.radius * sinO2 / (1 + sinO2);
  for (var n=0; n<nStates; n++) {
    var theta = omega*n + 0.5*Math.PI;
    var xBulb = this.x + (radius-rBulb) * Math.cos(theta);
    var yBulb = this.y - (radius-rBulb) * Math.sin(theta);
    this.lights[n] = new Light(this.paper, xBulb, yBulb, 0.8*rBulb, 0);
  }

  return this;
}

/*
 * Center Pod: Displays the center stuff.
 */
function CenterPanel(paper, x, y, radius) {

  // Constants.
  this.ENDPANEL      = -4
  this.CHOICE   = -3;

  this.ZERRIGHT = 1;
  this.ZERWRONG = 00;
  this.ONERIGHT = 11;
  this.ONEWRONG = 10;
  this.TWORIGHT = 21;
  this.TWOWRONG = 20;
  this.THRRIGHT = 31;
  this.THRWRONG = 30;
  this.FOURIGHT = 41;
  this.FOUWRONG = 40;
  this.ALLWRONG = 50;

  this.colIdle  = '#c0c0c0';
  this.colRight = '#b0ff80';
  this.colWrong = '#ff0000';

  // Initialize the status of the center panel.
  this.initStatus = function(status) {
    this.setStatus(0);
  }

  // Set the status of the center panel.
  this.setStatus = function(status) {
    if (this.icon.select("#idle") == null)
      return;

    // Because of loading delays, re-position the center.
    this.icon.transform("s" + this.amp * 3.6 + "," + this.amp * 3.6 + ",0,0" + "t" + this.xpos*.3 + "," + this.ypos*0);

    //this.icon.select("#idle").attr({'visibility' : 'hidden'});
    this.icon.select("#panel").attr({'visibility' : 'hidden'});

    switch (status) {
    case -3: // CHOICE
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
      this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
      this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
      this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
      //this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
      break;

    case 1: //RIGHT
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colRight});
      this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
      this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
      this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
      //this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
      var ChRight=true;
      break;
    case 11:
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
      this.icon.select("#oneCircle").attr({'fill' : this.colRight});
      this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
      this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
      //this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
      var ChRight=true;
      break;
    case 21:
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
      this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
      this.icon.select("#twoCircle").attr({'fill' : this.colRight});
      this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
      //this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
      var ChRight=true;
      break;
    case 31:
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
      this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
      this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
      this.icon.select("#thrCircle").attr({'fill' : this.colRight});
      //this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
      var ChRight=true;
      break;
    case 41:
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
      this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
      this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
      this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
      //this.icon.select("#fouCircle").attr({'fill' : this.colRight});
      var ChRight=true;
      break;

    case 00: //WRONG
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colWrong});
      this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
      this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
      this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
      //this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
      var ChRight=false;
      break;
    case 10:
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
      this.icon.select("#oneCircle").attr({'fill' : this.colWrong});
      this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
      this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
      //this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
      var ChRight=false;
      break;
    case 20:
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
      this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
      this.icon.select("#twoCircle").attr({'fill' : this.colWrong});
      this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
      //this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
      var ChRight=false;
      break;
    case 30:
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
      this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
      this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
      this.icon.select("#thrCircle").attr({'fill' : this.colWrong});
      //this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
      var ChRight=false;
      break;
    case 40:
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
      this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
      this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
      this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
      //this.icon.select("#fouCircle").attr({'fill' : this.colWrong});
      var ChRight=false;
      break;

    case 50:
      this.icon.select("#panel").attr({'visibility' : 'visible'});
      this.icon.select("#zerCircle").attr({'fill' : this.colWrong});
      this.icon.select("#oneCircle").attr({'fill' : this.colWrong});
      this.icon.select("#twoCircle").attr({'fill' : this.colWrong});
      this.icon.select("#thrCircle").attr({'fill' : this.colWrong});
      //this.icon.select("#fouCircle").attr({'fill' : this.colWrong});
      var ChRight=false;
      break;

    case -4:
      this.icon.select("#panel").attr({'visibility' : 'hidden'});
      break;
    }
  }

  this.visible  = paper;
  this.x      = x;
  this.y      = y;
  this.radius = radius;

  this.xpos = _P(69);
  this.ypos = _P(69);
  this.amp  = _P(this.radius/135);
  console.log("x: " + this.x + ", y: " + this.y + ", r: " + this.radius);

  // Helper to convert Snap.load() into a Promise.
  function loadSvg(url) {
    return new Promise(function(resolve, reject) {
      console.log("Loading: " + url + "...");
      Snap.load(url, resolve);
    });
  };

// -------------------------------------------------------------------------------

  this.icon = paper.group();
  loadSvg("img/choices.svg").then(
    function resolve(result) {
      this.icon.append(result);
      this.icon.attr({'visibility' : 'hidden'});
    }.bind(this),
    function reject(err) {
      console.log("Failure in loading");
    }
  );

  this.initStatus();

  return this;
}

/*
 * GameDisplay : displays a formula and a truth assignment as a
 * collection of plates with coloured light bulbs and
 * associated coloured switches.
 */
function GameDisplay(paper, x, y, radius, nClauses, nLiterals, nVars) {

  // Sets the formula of the game.
  this.setFormula = function(formula) {
    for (var n=0; n<formula.length; n++) {
      this.plates[n].setPlate(formula[n]);
    }
  };

  // Sets the status of the center panel.
  this.setCenterPanel = function(status) {
    this.centerPanel.setStatus(status);
  };

  // Make some plates flash with a special color.
  this.setFlashing = function(flashes) {
    for (var n=0; n<flashes.length; n++) {
      this.plates[n].setFlashing(flashes[n]);
    }
  }

  // Resets the flashing plates.
  this.resetFlashing = function(flashes) {
    for (var n=0; n<this.plates.length; n++) {
      this.plates[n].setFlashing(false);
    }
  }

  // Hide/Show.
  this.hide = function() {
    this.cover.attr({
      visibility: 'visible'
    });
  }
  this.show = function() {
    this.cover.attr({
      visibility: 'hidden'
    });
  }

  this.paper   = paper;
  this.x       = x;
  this.y       = y;
  this.radius  = radius;

  this.nClauses = nClauses;
  this.nLiterals = nLiterals;
  this.nVars = nVars;

  // Create a dummy formula for initialization.
  formula = Formula(nLiterals, nClauses, nVars);

  // Draw the plates.
  this.plates = Array();
  var omega = 2*Math.PI / nClauses;
  var sinO2 = Math.sin(omega/2);
  var rPlate = Math.min(0.35*this.radius, this.radius * sinO2 / (1 + sinO2));
  rPlate = rPlate < this.radius * 0.35? this.radius * 0.35 : rPlate;
  for (var n=0; n<nClauses; n++) {
    var theta = omega*n + 0.5*Math.PI;
    var xPlate = this.x + (radius-rPlate) * Math.cos(theta);
    var yPlate = this.y - (radius-rPlate) * Math.sin(theta);
    this.plates[n] = new Plate(this.paper, xPlate, yPlate, 0.8*rPlate, nVars, formula[n]);
  }

  // Draw a rectangle for hiding the display.
  this.cover = paper.rect(x-radius, y-radius, 2*radius, 2*radius);
  this.cover.attr({
    visibility: 'hidden',
    fill: 'white'
  });

  // Draw the center panel.
  this.centerPanel = new CenterPanel(this.paper, this.x, this.y, this.radius);

  return this;
}

/*
 * Circular Timer.
 */
var CircTimer = function(paper, x, y, r) {

  this.paper = paper;
  this.x = x;
  this.y = y;
  this.r = r;

  command = "M" + [x, y].join(",");
  this.arc = paper.path(command).attr({"stroke": "none", "fill": timerColor});

  this.setValue = function(value) {
    value = 0.998*value + 0.001;
    var theta = 2*Math.PI*value;
    var xpos = _P( this.x + this.r*Math.sin(theta) );
    var ypos = _P( this.y - this.r*Math.cos(theta) );
    path =
      "M" + [x, y].join(",")
      + "L" + [x, y-r].join(",")
      + "A" + [r, r, 180, +(value>0.5), 1, xpos, ypos].join(",")
      + "L" + [x, y].join(",") + "z";
    this.arc.attr({
      "path" : path,
    });
  };

  return this;
}

/*
 * Status bar.
 */
var StatusBar = function (paper, x, y, h) {

  this.setScore = function (score, color) {
    if (color == undefined)
      color = statusNormal;
    this.score.attr({"text": score, "fill" : color, "text-anchor" : "middle"});
  }

  this.x = x;
  this.y = y;
  this.h = h;
  this.score = paper.text(_P(x), _P(y), "")
  this.score.attr({
    "font": "Helvetica",
    "font-size": h,
  });

  return this;
}

/*
 * Horizontal meter bar.
 */
var MeterBar = function(paper, points, maxpoints, x, y, w, h) {

    // Draw the point cells.
    this.drawCells = function() {
        // Compute cell dimensions.
        cw = this.w / this.maxpoints;
        for (n=0; n<this.maxpoints; n++) {
            xpos = this.x + n * cw;
            ypos = this.y;
            this.cells[n] = this.paper.rect(xpos, ypos, 0.9*cw, 0.9*h, 0.1*cw);
            this.cells[n].attr({"fill" : "#000000"});
        }
    }

    // Sets the points.
    this.setPoints = function(points) {
        this.points = points;
        for (n=0; n<this.maxpoints; n++) {
            if (n < this.points)
                this.cells[n].attr({"fill" : "#000000"});
            else
                this.cells[n].attr({"fill" : "#dddddd"});
        }
    }

    // Initialize values.
    this.paper = paper;
    this.points = points;
    this.maxpoints = maxpoints;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.cells = Array();
    this.drawCells();
    this.setPoints(points)

    return this;
}



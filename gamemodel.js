
/******************************************************************************
 *
 * GAME MODEL
 *
 * This file should contain the model part of the game.
 *
 * (c) 2015-2016 Pedro A. Ortega <pedro.ortega@gmail.com>
 * (c) 2016      David N. White  <davey@autistici.org>
 *****************************************************************************/


/******************************************************************************
 * MODEL
 *****************************************************************************/

/*
 * Formula: a CNF formula.
 */
var Formula = function (nClauses, nLiterals, nVariables) {

  // Initialise the formula by setting all the literals to X1.
  this._init = function() {
    this.formula = [];
    for (var n=0; n<this.nClauses; n++) {
      this.formula[n] = [];
      for (var m=0; m<this.nLiterals; m++) {
        this.formula[n][m] = 1;
      }
    }
  }

  // Set the formula.
  this.set = function(form) {
    for (var n=0; n<this.nClauses; n++) {
      for (var m=0; m<this.nLiterals; m++) {
        this.formula[n][m] = form[n][m];
      }
    }
  }

  // Randomise the formula uniformly.
  this.random = function () {
    for (var n=0; n<this.nClauses; n++) {
      for (var m=0; m<this.nLiterals; m++) {
        this.formula[n][m] = Math.floor(this.nVariables*Math.random()) + 1;
        this.formula[n][m] = (Math.random() <= 0.5)?
          this.formula[n][m] : -this.formula[n][m];
      }
    }
  }

  // Do a random permutation of the formula's clauses.
  this.shuffle = function() {
    var currentIdx = this.formula.length;

    while (currentIdx != 0) {
      currentIdx--;
      randomIdx = Math.floor(currentIdx * Math.random());
      var temp = this.formula[currentIdx];
      this.formula[currentIdx] = this.formula[randomIdx];
      this.formula[randomIdx] = temp;
    }
  }

  // Evaluate the formula on the given states
  // and record the clauses that have been satisfied.
  // `states` : a Boolean array containing the truth assignments.
  // `satClauses` : a Boolean array of length that is equal to the
  //                formula's number of clauses. The satisfied claues
  //                will be recorded here.
  this.eval = function(states, satClauses) {
    nSat = 0;
    for (var n=0; n<this.nClauses; n++) {
      // Evaluate clause.
      var sat = false;
      for (var m=0; m<this.nLiterals; m++) {
        var xVal = this.formula[n][m];
        var xIdx = Math.abs(xVal) - 1;
        sat = sat || ((xVal > 0)? states[ xIdx ] : !states[ xIdx ]);
      }

      // Record clause value if required.
      if (satClauses)
        satClauses[n] = sat;

      // Update number of satisfied clauses.
      nSat = nSat + (sat? 1 : 0);
    }

    return nSat;
  }

  this.nClauses   = nClauses;
  this.nLiterals  = nLiterals;
  this.nVariables = nVariables;
  this.formula = [];
  this._init();

  return this;
}

/*
 * Assignment: Array of truth values.
 */
var Assignment = function (nVars) {

  this._init = function () {
    for (var n=0; n<this.nVars; n++)
      this.states[n] = false;
  }

  this.set = function (states) {
    for (var n=0; n<this.nVars; n++)
      this.states[n] = states[n];
  }

  this.toNumber = function () {
    p = 1;
    k = 0;
    for (var n=0; n<this.nVars; n++) {
      k = k + this.states[n]*p;
      p = p * 2;
    }
    return k;
  }

  this.nVars  = nVars;
  this.states = [];
  this._init();

  return this;
}


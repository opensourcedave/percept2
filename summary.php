<?php
//
// Summary page of experimental progress.
// (c) 2016 Pedro A. Ortega <pedro.ortega@gmail.com>
// (c) 2016 David N. White  <davey@autistici.org>
//

$table = "percep2";

function results_table($table) {
  // Connect to the database.
  include('database_connect.php');
  
  // Get the results.
  $query = "SELECT COUNT(*) AS `Rows`, COUNT(DISTINCT `levelId`) AS `Levels`, (MAX(`timestamp`) - MIN(`timestamp`)) / 60 AS `Time`, MAX(`timestamp`) AS `Last`, `userId` FROM `".$table."` GROUP BY `userId` ORDER BY `userId`";
  $result = mysql_query($query) or die(mysql_error());

  printf("<table>\n");
  printf("\t<tr>");
  printf("<td>Number</td>");
  printf("<td>Rows</td>");
  printf("<td>Levels</td>");
  printf("<td>Time</td>");
  printf("<td>Last</td>");
  printf("<td>userId</td>");
  printf("</tr>\n");
  $n = 1;
  while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
    
    $last = (time() - $row["Last"]) / 60;
    
    printf("\t<tr>");
    printf("<td>%d</td>", $n);
    printf("<td>%d</td>", $row["Rows"]);
    printf("<td>%d</td>", $row["Levels"]);
    printf("<td>%d</td>", $row["Time"]);
    if ($last <= 10)
      printf("<td><strong>%d</strong></td>", $last);
    else
      printf("<td>%d</td>", $last);
    if ($row["Levels"] >= 5)
      printf("<td><strong>%s</strong></td>", $row["userId"]);
    else
      printf("<td>%s</td>", $row["userId"]);
    printf("</tr>\n");
    $n++;
  }
  printf("</table>\n");
}
?>

<html>
  <head>
    <title>Progress Summary Percep 2</title>
  </head>
  <body>
    <h1>Progress Summary Percep 2</h1>
    <?php
      results_table($table);
    ?>
  </body>
</html>






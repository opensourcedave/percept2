/******************************************************************************
 *
 * GAME CONTROLLER
 *
 * (c) 2015-2016 Pedro A. Ortega <pedro.ortega@gmail.com>
 * (c) 2015      David N. White  <davey@autistici.org>
 *
 *****************************************************************************/

/******************************************************************************
 * CONTROLLER
 *****************************************************************************/

/*
 * Configuration of the game.
 */
conf = {
  table        : "percep2",
  duration     : 2000,
  endDuration  : 1000,
  interstim    : 1000,
  intertrial   : (function() { return 500 + 500*Math.random() })(),
  startLives   : 16
}

// Random probably-unique identifier.
function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

// Shuffle array.
shuffleArray = function(arr) {
  var currentIdx = arr.length;

  while (currentIdx != 0) {
    currentIdx--;
    randomIdx = Math.floor(currentIdx * Math.random());
    var temp  = arr[currentIdx];
    arr[currentIdx] = arr[randomIdx];
    arr[randomIdx]  = temp;
  }
}


var Game = function(targetDisplay, nClauses, nLiterals, nVars, data) {

// -------------------------------------------------------------------------------
//   * GAME STRUCTURE   *


  // Initialize game and start it.
  this.startGame = function() {
    this.level = 0;


    // Record start of experiment.
    this.trialData = {
      userId     : this.user,
      levelId    : -1,
      trialId    : -1,
      duration   : -1,
      refId      : -1,
      refForm    : "",
      cmpId      : -1,
      refRT      : -1,
      choiceId   : -1,
      success    : -1,
      timestamp  : Math.floor(Date.now() / 1000),
    };

    this.userData.push(this.trialData); //contains all data //push add to end of array
    this.saveDB(this.userData);

    // Start the level.
    this.levelStart();  //start
  }

// -------------------------------------------------------------------------------

  // Finish game.
  this.endGame = function() {
    this.cut();
    this.splashScreen(
      "GAME OVER",
      "<p>Thank you! Your code is: " + this.user + "</p>",
      this.endGame.bind(this));
    console.log(this.userData);
  }

// -------------------------------------------------------------------------------
//    LEVELS           *

  // Start level.
  this.levelStart = function() {
    // Reset lives, trial number, and level size.
    this.lifes = conf.startLives;
    this.trial = 0;
    this.levelSize = this.data.level[ this.level ].trials.length;

    // Show a splash screen.
    var msg = this.data.level[this.level].message
      + "<p><h3><strong>Your code is " + this.user + "</strong></h3></p>"
      + "<p>[Press any key to start.]</p>"
    this.cut();
    //this.setStatus("Level " + (this.level + 1));
    this.setStatus("");
    $("#container").append(msg);

    // Shuffle the trials.
    shuffleArray(this.data.level[this.level].trials);

    // Display lifes.
    this.lifebar.setPoints(this.lifes);

    // Activate spacebar press.
    document.onkeypress = this.levelBeginLoop.bind(this); //loops levels
  }

// -------------------------------------------------------------------------------

  // Begin the trial loop.
  this.levelBeginLoop = function(e) {
    e = e || window.event;

    // Restore display.
    $("#container").children().remove();
    $("#container").text("");
    this.paste();
    this.display.hide();

    // Deactivate key press.
    document.onkeypress = null;

    // Go to main level handler.
    setTimeout(this.levelLoop.bind(this), 0);
  }

// -------------------------------------------------------------------------------

  // Continue level.
  this.levelLoop = function() {

    this.setStatus("Level " + (this.level + 1) + " - Round " + (this.trial + 1) );

    // Death.
    if (this.lifes == 0) {
      this.setStatus("Please repeat level.");
      setTimeout(this.levelStart.bind(this), 3000); //identity of the ownder of control, this always reffers to controller
      return;
    }

    // Next level.
    if (this.trial == this.levelSize) {
      this.setStatus("Level " + (this.level + 1) + " completed!");
      setTimeout(this.levelEnd.bind(this), 1000);
      return;
    }

    // Next trial.
    trialId = this.data.level[ this.level ].trials[ this.trial ];
    // Prepare trial data structure.
    this.trialData = {
      userId     : this.user,
      levelId    : this.level,
      trialId    : this.trial,
      duration   : conf.duration,
      refId      : this.data.level[ this.level ].trials[ this.trial ].refId,
      refForm    : new Formula(6, 2, 3),
      refRT      : 0,
      choiceId   : 0,
      success    : 0,
      timestamp  : Math.floor(Date.now() / 1000),
    };
    this.trialData.refForm.set(
      this.data.level[ this.level ].trials[ this.trial ].refForm);
    //this.trialData.cmpForm.set(
    //  this.data.level[ this.level ].trials[ this.trial ].cmpForm);
    this.trial++;

    setTimeout(this.trialShowRef.bind(this), conf.interstim);
  }

// -------------------------------------------------------------------------------

  // End level.
  this.levelEnd = function() {
    // Save data.
    this.saveFile(this.user + ".csv", JSON.stringify(this.userData));
    this.saveDB(this.userData);

    // Start next level if end not reached.
    this.level++;
    totalLevels = this.data.level.length;
    if (this.level < totalLevels) {
      this.levelStart();
      return;
    }

    // End game.
    this.endGame();
  }

// -------------------------------------------------------------------------------
// TRIAL FRAMES : COMMUNICATION VIA this.trialData

  // Stages of the Trial:
  // PRECONDITION:  this.trialData must contain trial parameters.
  // POSTCONDITION: this.trialData will contain user choices.
  // 1- trialShowRef
  // 2- trialBetweenStim

  // 5- trialChoice
  // 5a- trialChoiceProcess
  // 6- trialChoiceWarn
  // 6a- trialChoiceProcess
  // 7- trialRevealResult

  // 8- trialEnd

// -------------------------------------------------------------------------------

  // First step: show initial stimulus.
  this.trialShowRef = function() {
    // Set status.
    this.setStatus(
      "Level " + (this.level + 1) + " - Round " + (this.trial)
    );

    // Display stimulus.
    this.display.show();
    this.trialData.refForm.shuffle();
    this.display.setFormula(this.trialData.refForm.formula);
    //this.display.setCenterPanel(this.display.centerPanel.ONE);

    // Display lifes.
    this.lifebar.setPoints(this.lifes);

    // Remove warning.
    this.timer.setValue(0);

    // Set timeout for showing the reference.
    setTimeout(
      this.trialBetweenStim.bind(this),
      conf.duration
    );
  }

// -------------------------------------------------------------------------------


  this.trialBetweenStim = function(event) {
    // Hide current stimulus.
    this.display.hide();

    // Set timeout for next trial stage.
    setTimeout(
      this.trialChoice.bind(this),
      conf.interstim
    );
  }

// -------------------------------------------------------------------------------

  this.trialChoice = function() {

    // Display comparative stimulus.
    this.display.hide();

    //this.display.setCenterPanel(this.display.centerPanel.TWO);

    // Initialize choice.
    this.trialData.choiceId = 0;
    this.display.setCenterPanel(this.display.centerPanel.CHOICE);


    // Initiate reaction time timer.
    this.timerStart = Date.now();

    // Activate keypress.
      document.onkeydown = this.trialChoiceProcess.bind(this);
  }


// -------------------------------------------------------------------------------

  // Register choice and update center panel.
  this.trialChoiceProcess = function(e) {
    e = e || window.event;
    if (e.keyCode == '37') {
      //left
      this.trialData.choiceId = 0;
    } else if (e.keyCode == '38') {
      //up
      this.trialData.choiceId = 1;
    } else if (e.keyCode == '39') {
      //right
      this.trialData.choiceId = 2;
    } else if (e.keyCode == '40') {
      //down
      this.trialData.choiceId = 3;
    } else {
      this.trialData.choiceId = 5;
    }
    //if (e.keyCode == '32') {
    //  //space
    //  this.trialData.choiceId = 4;
    //  vali = true;
    //}

    setTimeout(
      this.trialRevealResult.bind(this),
      0
      );
  }

// -------------------------------------------------------------------------------

  // Reveal the result of the choice.
  this.trialRevealResult = function() {
    // Deactivate keypress.
    document.onkeydown = null;

    // Record reaction time.
    this.timerEnd = Date.now();
    this.trialData.refRT = this.timerEnd - this.timerStart;
    console.log("RT: " + this.trialData.refRT);

// -------------------------------------------------------------------------------

    // Calculate and display result.

    var equal = (this.trialData.refId == this.trialData.choiceId);

    if ((this.trialData.choiceId == 0) && equal) {
      this.display.setCenterPanel(this.display.centerPanel.ZERRIGHT);
      this.trialData.success = 1;
    }
    else if ((this.trialData.choiceId == 0) && !equal) {
      this.display.setCenterPanel(this.display.centerPanel.ZERWRONG);
      this.trialData.success = 0;
      this.lifes--;
    }
    else if ((this.trialData.choiceId == 1) && equal) {
      this.display.setCenterPanel(this.display.centerPanel.ONERIGHT);
      this.trialData.success = 1;
    }
    else if ((this.trialData.choiceId == 1) && !equal) {
      this.display.setCenterPanel(this.display.centerPanel.ONEWRONG);
      this.trialData.success = 0;
      this.lifes--;
    }
    else if ((this.trialData.choiceId == 2) && equal) {
      this.display.setCenterPanel(this.display.centerPanel.TWORIGHT);
      this.trialData.success = 1;
    }
    else if ((this.trialData.choiceId == 2) && !equal) {
      this.display.setCenterPanel(this.display.centerPanel.TWOWRONG);
      this.trialData.success = 0;
      this.lifes--;
    }
    else if ((this.trialData.choiceId == 3) && equal) {
      this.display.setCenterPanel(this.display.centerPanel.THRRIGHT);
      this.trialData.success = 1;
    }
    else if ((this.trialData.choiceId == 3) && !equal) {
      this.display.setCenterPanel(this.display.centerPanel.THRWRONG);
      this.trialData.success = 0;
      this.lifes--;
    }
    else if (this.trialData.choiceId == 5) {
      this.display.setCenterPanel(this.display.centerPanel.ALLWRONG);
      this.trialData.success = 0;
      this.lifes--;
    }
    ////Currently 3 == 4
    //else if ((this.trialData.choiceId == 3) && (this.trialData.refId == 4)) {
    //  this.display.setCenterPanel(this.display.centerPanel.THRRIGHT);
    //  this.trialData.success = 1;
    //  this.lifes--;
    //}
    //Currently 3 == 4
    //else if ((this.trialData.choiceId == 4) && equal) {
    //  this.display.setCenterPanel(this.display.centerPanel.FOURIGHT);
    //  this.trialData.success = 1;
    //}
    //else if ((this.trialData.choiceId == 4) && !equal) {
    //  this.display.setCenterPanel(this.display.centerPanel.FOUWRONG);
    //  this.trialData.success = 0;
    //  this.lifes--;
    //}

    // Update lifes.
    this.lifebar.setPoints(this.lifes);

    // Set timeout for end of trial.
    setTimeout(
      this.trialEnd.bind(this),
      1000
    );
  }

// ==============================================================================
// ==============================================================================

  this.trialEnd = function() {

    // Hide Stimulus.
    this.display.hide();

    // Set Idle center.
    this.display.setCenterPanel(this.display.centerPanel.ENDPANEL);

    // Record user data.
    console.log(this.trialData);
    this.userData.push(this.trialData);

    // Back to level processing.
    setTimeout(this.levelLoop.bind(this), 0);
  }

  /***********************
   * DISPLAY CUT & PASTE *
   ***********************/

  // Cut/Paste the game from the DOM tree.
  this.cut = function(){
    this.snapshot = $("#container").children().detach();
  }
  this.paste = function() {
    $('#container').append(this.snapshot);
  }

  /****************************
   * STATUS AND SPLASH SCREEN *
   ****************************/
  this.splashCallback;
  this.splashScreen = function(title, msg, callback) {
    this.cut();
    this.setStatus(title);
    $("#container").append(msg);
    this.splashCallback = callback;
  }

  this.splashScreenEnd = function() {
    $("#container").children().remove();
    $("#container").text("");
    this.paste();
    setTimeout(this.splashCallback, 0);
  }

  this.setStatus = function(txt) {
    $('#status').text(txt);
  }

  /****************************
   * DATA STORAGE             *
   ****************************/

  // Save data to file.
  this.saveFile = function(filename, filedata) {
    $.ajax({
      type:  "post",
      cache: false,
      async: false,
      url:   "file_save.php",
      data:  {filename: filename, filedata: filedata},
    });
  }

  // Save data to database.
  this.saveDB = function(userData) {
    $.ajax({
      type:   "post",
      cache:  false,
      async:  false,
      url:    "database_save.php",
      data:   {
        table: conf.table,
        json:  JSON.stringify(userData),
      },
      success:  function(output) { console.log(output); },
      complete: this.saveDBComplete.bind(this),
    });
  }

  this.saveDBComplete = function(event, request, settings) {
    this.userData = [];
  }


  /************************
   * PROPERTIES AND STATE *
   ************************/

  // Snapshot of current display for cutting/pasting the graphics.
  this.snapshot;

  // Formula structure.
  this.nClauses  = nClauses;
  this.nLiterals = nLiterals;
  this.nVars     = nVars;

  // Data and user data.
  this.user = randomString(10, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
  this.data = data;
  this.userData = [];

  // Current formula, truth value assignment, and correct clauses.
  this.form    = new Formula(this.nClauses, this.nLiterals, this.nVars);

  // Graphical objects: paper, game, timer, and life bar.
  this.targetDisplay = "#canvas";
  $("#container").append('<svg id="canvas" xmlns="http://www.w3.org/2000/svg" version="1.1"></svg>');
  var paper    = Snap(this.targetDisplay);
  this.width   = $("#canvas").width();
  this.height  = $("#canvas").height();
  var radius   = this.width / 6;
  var xpos     = this.width / 2;
  var ypos     = this.height / 2.8 ;
  this.timer   = new CircTimer(paper, xpos, ypos, radius);
  this.display = new GameDisplay(paper, xpos, ypos, radius,
               nClauses, nLiterals, nVars);

  var barh     = this.height / 10;
  var barw     = 0.50 * this.width;
  var xpos     = 0.25 * this.width;
  var ypos     = this.height - 2*barh;
  this.lifebar = new MeterBar(paper, conf.startLives, conf.startLives, xpos, ypos, barw, barh);

  // Player stats game state.
  this.lifes = 0;
  this.trial = 0;
  this.levelSize = 0;
  this.endTime = 0;

  this.display.setCenterPanel(0);

  return this;
}



/******************************************************************************
 *
 * GAME CONTROLLER - TEACHING VERSION.
 *
 * (c) 2015-2016 Pedro A. Ortega <pedro.ortega@gmail.com>
 * (c) 2016      Davd N. White <davey@autistici.org>
 *****************************************************************************/

/******************************************************************************
 * CONTROLLER
 *****************************************************************************/

var TeachingGame = function(targetDisplay, nClauses, nLiterals, nVars, formulas) {

  this.startGame = function() {
    // Reset display.
    this.display.resetFlashing();
    this.display.setActive(true);
    this.shuffleFormula();
    this.form.shuffle();
    this.display.setFormula(this.form.formula);
    this.display.resetControls();

    // Activate keypress.
    document.onkeydown = this.arrowKeysCallback.bind(this);

    // Evaluate and set flash.
    //this.evaluateAssignment();
  };

  // Ends the trial. Requires a well-defined callback.
  this.endTrial = function() {
    // Complete the timer.
    this.display.setActive(false);

    // Deactivate the keypress.
    document.onkeydown = null;

    // Set the next timeout for the end of the trial.
    setTimeout(this.interTrialStart.bind(this), conf.endDuration);

    // Evaluate the results of the trial.
    var solution = new Assignment(this.nVars);
    solution.set(this.display.getControls());

    // Advance to next trial.
    this.trial++;
  }

  this.interTrialStart = function() {
    if (this.lifes > 0 && this.trial < this.levelSize) {
      // Hide display.
      this.cut();
      setTimeout(this.interTrialEnd.bind(this), conf.intertrial);
    } else {
      this.mainLevel();
    }
  }

  this.interTrialEnd = function() {
    // Unhide display.
    this.paste()
    this.mainLevel();
  }

  /********************
   * HANDLE KEYPRESS  *
   ********************/
  this.arrowKeysCallback = function(e){
    // Capture the keypress.
    e = e || window.event;
    e.preventDefault();
    if (e.keyCode == '37') // LEFT
      this.display.controls.switches[1].click();
    else if (e.keyCode == '38') // UP
      this.display.controls.switches[0].click();
    else if (e.keyCode == '39') // RIGHT
      this.display.controls.switches[2].click();

    // Evaluate the formula.
    this.evaluateAssignment();
  }

  /**********************************
   * EVALUATION OF TRUTH ASSIGNMENT *
   **********************************/

  this.evaluateAssignment = function() {
    // Evaluate the formula.
    var solution = new Assignment(this.nVars);
    solution.set(this.display.getControls());
    this.form.eval(solution.states, this.correct);
    this.display.setFlashing(this.correct);
  }

  /*******************
   * SHUFFLE FORMULA *
   *******************/

  this.shuffleFormula = function() {
    this.form.formula = formulas[ Math.floor( formulas.length * Math.random() ) ];
  }

  /************************
   * PROPERTIES AND STATE *
   ************************/

  // Formula structure.
  this.nClauses  = nClauses;
  this.nLiterals = nLiterals;
  this.nVars     = nVars;

  // Current formula, truth value assignment, and correct clauses.
  this.form    = new Formula(this.nClauses, this.nLiterals, this.nVars);
  this.assign  = new Assignment(this.nVars);
  this.correct = ArrayWithValue(this.nClauses, false);

  this.formulas = formulas;


  // Graphical objects: paper, game, timer, and life bar.
  this.targetDisplay = targetDisplay;
  $(this.targetDisplay).append('<svg id="canvas" xmlns="http://www.w3.org/2000/svg" version="1.1"></svg>');
  var paper    = Snap("#canvas");
  this.width   = $("#canvas").width();
  this.height  = $("#canvas").height();
  var radius   = this.width / 2;
  var xpos     = radius;
  var ypos     = radius;
  this.display = new GameDisplay(paper, xpos, ypos, radius,
               this.form.formula, this.assign.states);

  // Add a "NEXT" button.
  this.next = paper.group();
  Snap.load("img/next.svg",
    function ( loadedFragment ) {
      this.next.append( loadedFragment );
      this.next.attr({
        "cursor": "pointer",
      });
      this.next.click((function handler() { this.startGame() }).bind(this));
  }, this);
  this.next.transform("t" + 0.9*this.width + "," + 0.9*this.height);

  var barh     = (this.height-this.width) / 3;
  var barw     = 0.50 * this.width;
  var xpos     = 0.25 * this.width;
  var ypos     = this.width + barh;

  // Player stats game state.
  this.trial = 0;

  return this;
}



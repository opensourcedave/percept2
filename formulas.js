
// Formula table.
var ftable = [
  [[1,-3],[-3,-1],[3,-2],[3,-1],[2,-1],[-3,-2]], // b b b | 8*
  [[-1,-3],[-3,1],[3,-2],[3,1],[2,1],[-3,-2]],   // b w b | 6*
  [[-1,-3],[-3,1],[3,2],[3,1],[-2,1],[-3,2]],    // w w b | 5*
  [[-1,3],[3,1],[-3,-2],[-3,1],[2,1],[3,-2]],    // b w w | 5*
  [[1,-3],[-3,-1],[3,2],[3,-1],[-2,-1],[-3,2]],  // w b b | 7
  [[1,3],[3,-1],[-3,-2],[-3,-1],[2,-1],[3,-2]],  // b b w | 7
  [[1,3],[3,-1],[-3,2],[-3,-1],[-2,-1],[3,2]],   // w b w | 6
  [[-1,3],[3,1],[-3,2],[-3,1],[-2,1],[3,2]]      // w w w | 4
];

// Procedure to invert the formulas.
function invertFormulas(F) {
  for (n=0; n<F.length; n++) {
    for (m=0; m<F[n].length; m++) {
      for (l=0; l<F[n][m].length; l++)
        F[n][m][l] = -F[n][m][l];
    }
  }
}

//invertFormulas(ftable);

// Build trials.
var trials = [];
for (var m=0; m<4; m++) { // Diagonal.
  trial = {
    "refId"   : m,
    "cmpId"   : m,
    "refForm" : ftable[m],
    "cmpForm" : ftable[m]
  };
  trials.push(trial);
  trials.push(trial);
  trials.push(trial);
}
for (var m=0; m<4; m++) { // Off-diagonal.
  for (var n=0; n<4; n++) {
    trial = {
      "refId"   : m,
      "cmpId"   : n,
      "refForm" : ftable[m],
      "cmpForm" : ftable[n]
    };
    trials.push(trial);
  }
}
console.log(trials);

// Game levels.
var level = [];
for (var n=1; n<=10; n++) {
  level.push({
    "name" : "Level " + n,
    "message" : "",
    "trials"  : trials,
  });
}

level[0].message = "<p><h1><br><br><br><br>The Game is about to start.<br><br></h1></p><p align=center> Please read the instructions in the previous window, if you havn't already.</p><p align=center>You <strong>will</strong> find the task difficult in the beginning, but as you get familiar with it, you <strong>will</strong> improve<br><br><br></p>";
level[1].message = "<p>Well done! Do this again.</p>";
level[2].message = "<p>Very good! Do this again.</p>";
level[3].message = "";
level[4].message = "";
level[5].message = "<p>Very impressive! You have finished half of the game.</p>";
level[6].message = "<p>Impressive! Keep going!</p>";
level[7].message = "";
level[8].message = "";
level[9].message = "<p>Almost done!</p>";


// END OF FILE.

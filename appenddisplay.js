// -------------------------------------------------------------------------------

  // Constants.
  this.ONE      = -1;
  this.TWO      = -2;
  this.CHOICE   = -3;

  this.ZERRIGHT = 01;
  this.ZERWRONG = 00;
  this.ONERIGHT = 11;
  this.ONEWRONG = 10;
  this.TWORIGHT = 21;
  this.TWOWRONG = 20;
  this.THRRIGHT = 31;
  this.THRWRONG = 30;
  this.FOURIGHT = 41;
  this.FOUWRONG = 40;

  this.IDLE     = -1;

// -------------------------------------------------------------------------------
case -1: // ONE
  this.icon.select("#one").attr({'visibility' : 'visible'});
  break;
case -2: // TWO
  this.icon.select("#two").attr({'visibility' : 'visible'});
  break;
case -3: // CHOICE
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
  this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
  this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
  this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
  this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
  break;

case 01: //RIGHT
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colRight});
  this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
  this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
  this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
  this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
  var ChRight=true;
  break;
case 11:
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
  this.icon.select("#oneCircle").attr({'fill' : this.colRight});
  this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
  this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
  this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
  var ChRight=true;
  break;
case 21:
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
  this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
  this.icon.select("#twoCircle").attr({'fill' : this.colRight});
  this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
  this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
  var ChRight=true;
  break;
case 31:
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
  this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
  this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
  this.icon.select("#thrCircle").attr({'fill' : this.colRight});
  this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
  var ChRight=true;
  break;
case 41:
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
  this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
  this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
  this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
  this.icon.select("#fouCircle").attr({'fill' : this.colRight});
  var ChRight=true;
  break;

case 00: //WRONG
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colWrong});
  this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
  this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
  this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
  this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
  var ChRight=false;
  break;
case 10:
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
  this.icon.select("#oneCircle").attr({'fill' : this.colWrong});
  this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
  this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
  this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
  var ChRight=false;
  break;
case 20:
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
  this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
  this.icon.select("#twoCircle").attr({'fill' : this.colWrong});
  this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
  this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
  var ChRight=false;
  break;
case 30:
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
  this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
  this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
  this.icon.select("#thrCircle").attr({'fill' : this.colWrong});
  this.icon.select("#fouCircle").attr({'fill' : this.colIdle});
  var ChRight=false;
  break;
case 40:
  this.icon.select("#panel").attr({'visibility' : 'visible'});
  this.icon.select("#zerCircle").attr({'fill' : this.colIdle});
  this.icon.select("#oneCircle").attr({'fill' : this.colIdle});
  this.icon.select("#twoCircle").attr({'fill' : this.colIdle});
  this.icon.select("#thrCircle").attr({'fill' : this.colIdle});
  this.icon.select("#fouCircle").attr({'fill' : this.colWrong});
  var ChRight=false;
  break;

default: // IDLE
  this.icon.select("#idle").attr({'visibility' : 'visible'});
  break;
// -------------------------------------------------------------------------------

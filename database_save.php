<?php
//
// Saves data into DB.
// (c) 2015 Pedro A. Ortega <pedro.ortega@gmail.com>
// (c) 2016 David N. White  <davey@autistici.org>
// Submits data to a mySQL database.
//

// Connect to the database.
include('database_connect.php');

// Insert operation.
function mysql_insert($table, $inserts) {
  // Clean values and insert them into the database.
  $values = array_map('mysql_real_escape_string', array_values($inserts));
  $keys = array_keys($inserts);
  $query = 'INSERT INTO `'.$table.'` (`'.implode('`,`', $keys).'`) VALUES (\''. implode('\',\'', $values).'\')';
  //echo $query . '\n';

  $result = mysql_query($query) or die(mysql_error());
}

// Get the table name.
$tab = $_POST['table'];

// Decode the data object from json.
$trials = json_decode($_POST['json']);

//var_dump($trials);

// For each element in the trials array, insert the row into the mysql table.
for ($i=0; $i<count($trials);$i++) {
  $to_insert = (array)($trials[$i]);
  $result = mysql_insert($tab, $to_insert);
}

echo "Successful insert.\n"

?>
